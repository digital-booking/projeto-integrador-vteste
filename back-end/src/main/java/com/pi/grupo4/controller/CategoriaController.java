package com.pi.grupo4.controller;


import com.pi.grupo4.model.dto.CategoriaDTO;
import com.pi.grupo4.service.impl.CategoriaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

    @Autowired
    private CategoriaServiceImpl categoriaService;

    @PostMapping("/salvar")
    public ResponseEntity salvarDentista(@RequestBody CategoriaDTO categoriaDTO) {
        return ResponseEntity.ok(categoriaService.save(categoriaDTO));

    }

    @GetMapping("/buscartodos")
    public ResponseEntity<List<CategoriaDTO>> findAll() {
        return ResponseEntity.ok(categoriaService.findAll());
    }

    @GetMapping("/buscar/{id}")
    public ResponseEntity findBYId(@PathVariable Long id) {
        CategoriaDTO categoryDTO = categoriaService.findById(id);
        return ResponseEntity.ok(categoryDTO);
    }

    @GetMapping("/deletar/{id}")
    public void deletar(@PathVariable Long id) {
            categoriaService.findById(id);
            categoriaService.delete(id);
    }

    @PutMapping("/atualizar/{id}")
    public ResponseEntity atualizar(@PathVariable Long id ,@RequestBody CategoriaDTO categoriaDTO) {
        return ResponseEntity.ok(categoriaService.update(categoriaDTO));
    }

}
