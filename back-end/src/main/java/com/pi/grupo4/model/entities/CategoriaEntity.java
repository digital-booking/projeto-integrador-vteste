package com.pi.grupo4.model.entities;

import com.pi.grupo4.model.dto.CategoriaDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="Categoria")
public class CategoriaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="qualificacao")
    private String qualificacao;

    @Column(name="descricao")
    private String descricao;

    @Column(name="imagem")
    private String imagem;

    public CategoriaEntity() {
    }

    public CategoriaEntity(Long id, String qualificacao, String descricao, String imagem) {
        this.id = id;
        this.qualificacao = qualificacao;
        this.descricao = descricao;
        this.imagem = imagem;
    }

    public CategoriaEntity(CategoriaDTO categoriaDTO) {
        this.id = categoriaDTO.getId();
        this.qualificacao = categoriaDTO.getQualificacao();
        this.descricao = categoriaDTO.getDescricao();
        this.imagem = categoriaDTO.getImagem();
    }


}