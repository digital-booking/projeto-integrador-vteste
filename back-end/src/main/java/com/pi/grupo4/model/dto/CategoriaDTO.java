package com.pi.grupo4.model.dto;

import com.pi.grupo4.model.entities.CategoriaEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriaDTO {

    private Long id;
    private String qualificacao;
    private String descricao;
    private String imagem;


    public CategoriaDTO() {
    }

    public CategoriaDTO(CategoriaEntity category) {
        this.id = category.getId();
        this.qualificacao = category.getQualificacao();
        this.descricao = category.getDescricao();
        this.imagem = category.getImagem();
    }
}