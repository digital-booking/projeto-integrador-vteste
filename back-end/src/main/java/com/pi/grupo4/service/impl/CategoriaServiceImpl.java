package com.pi.grupo4.service.impl;

import com.pi.grupo4.model.dto.CategoriaDTO;
import com.pi.grupo4.model.entities.CategoriaEntity;
import com.pi.grupo4.repository.CategoriaRepository;
import com.pi.grupo4.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class CategoriaServiceImpl implements CrudService<CategoriaDTO> {

    @Autowired
    private CategoriaRepository categoriaRepository;


    @Override
    public ResponseEntity save(CategoriaDTO categoriaDTO){
        CategoriaEntity categoriaEntity;
        categoriaEntity = new CategoriaEntity(categoriaDTO);

        return ResponseEntity.ok().body(categoriaRepository.save(categoriaEntity));

    }
    @Override
    public List<CategoriaDTO> findAll() {
        List<CategoriaEntity> categorialist = categoriaRepository.findAll();
        List<CategoriaDTO> categoriaDTOList = new ArrayList<>();
            for (CategoriaEntity categoriaEntity : categorialist) {
                CategoriaDTO categoriaDTO = new CategoriaDTO(categoriaEntity);
                categoriaDTOList.add(categoriaDTO);
            }
            return categoriaDTOList;
        }

    @Override
    public CategoriaDTO findById(Long id) {
        return new CategoriaDTO(categoriaRepository.getById(id));
    }


    @Override
    public void delete(Long id) {
            categoriaRepository.deleteById(id);
        }

    @Override
    public CategoriaDTO update(CategoriaDTO categoriaDTO) {
        CategoriaEntity categoriaBase = categoriaRepository.getById(categoriaDTO.getId());

        if(categoriaDTO.getDescricao()!=null) {
            categoriaBase.setDescricao(categoriaDTO.getDescricao());
        }
        if(categoriaDTO.getQualificacao()!=null) {
            categoriaBase.setQualificacao(categoriaDTO.getQualificacao());
        }
        if(categoriaDTO.getImagem()!=null) {
            categoriaBase.setImagem(categoriaDTO.getImagem());
        }

        categoriaRepository.saveAndFlush(categoriaBase);
        CategoriaDTO categoriaDTO1 = new CategoriaDTO(categoriaBase);


        return categoriaDTO1;
    }
}
