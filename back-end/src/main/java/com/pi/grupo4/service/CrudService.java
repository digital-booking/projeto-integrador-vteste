package com.pi.grupo4.service;

import com.pi.grupo4.model.entities.CategoriaEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface CrudService<T>{

    ResponseEntity save(T t) ;
    List<T> findAll();
    T findById(Long id);
    void delete(Long id);
    T update(T t);
}