package com.pi.grupo4.repository;

import com.pi.grupo4.model.entities.CategoriaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<CategoriaEntity,Long> {


}
