import { Routes, Route } from 'react-router-dom';
import Home from '../pages/Home';
import Account from '../pages/Account';
import Login from '../pages/Login';


const RouteList = () => (

  <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/account" element={<Account />} />
          <Route path="/login" element={<Login />} />
  </Routes>
);

export default RouteList;