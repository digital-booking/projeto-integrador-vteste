import './App.scss';
import Header from './components/header'
import Footer from './components/footer';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (

    <Router>
      <Header />
      <Footer />
    </Router>

  );
}

export default App;
