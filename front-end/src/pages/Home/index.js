/* import Search from '../../components/Search'; */
import Category from '../../components/Category';
import ProductList from '../../components/ProductList';


const Home = () => {

  return (
    <>
      {/*       <Search /> */}
      <Category />
      <ProductList />
    </>
  );
}

export default Home;