import * as Yup from 'yup';

const SignupSchema = Yup.object().shape({
  firstName: Yup.string().required('Este campo é obrigatório'),
  lastName: Yup.string().required('Este campo é obrigatório'),
  email: Yup.string().email('E-mail inválido').required('Este campo é obrigatório'),
  password: Yup.string().min(6,'Deve ter no mínimo 6 caracteres').required('Este campo é obrigatório'),
  confirmPassword: Yup.string().required('Este campo é obrigatório'),
});

export default SignupSchema;