import React from 'react';
import './style.scss';
import { Link } from "react-router-dom";
import { Formik, Form  } from 'formik';
import SignupSchema from './validation';
import Field from '../../components/Form/Field';


const Account = () => {

  return (
    <section className='create-account'>
      <h1>
        Criar conta
      </h1>

      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          email: '',
          password: '',
          confirmPassword: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={values => {
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <Form className='form-account'>
          
            <div className='input-group__name'>
              <div className='input-group__name__full-name'>
                <div className='input-group'>
                  <Field 
                    name="firstName" 
                    type="text" 
                    className='input-group__input' 
                    label="Nome"
                  />
                  {errors.firstName && touched.firstName ? (
                  <div className='field-errors '>{errors.firstName}</div>) : null}
                </div>
                
                <div className='input-group'>
                  <Field 
                    name="lastName" 
                    type="text" 
                    className='input-group__input' 
                    label="Sobrenome"
                  />
                  {errors.lastName && touched.lastName ? (<div className='field-errors '>{errors.lastName}</div>) : null}
                </div>
              </div>
            </div>

            <div className='input-group'>
              <Field 
                name="email" 
                type="email" 
                className='input-group__input' 
                label="E-mail"
              />
              {errors.email && touched.email ? <div className='field-errors'>{errors.email}</div> : null}
            </div>

            <div className='input-group'>
              <Field 
                name="password" 
                type="password" 
                className='input-group__input'  
                label="Senha"
              />
              {errors.password && touched.password ? <div className='field-errors'>{errors.password}</div> : null}
            </div>

            <div className='input-group'>
              <Field 
                name="confirmPassword" 
                type="password" 
                className='input-group__input'  
                label="Confirmar senha"
              />
              {errors.confirmPassword && touched.confirmPassword ? <div className='field-errors'>{errors.confirmPassword}</div> : null}
            </div>

            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>

      <div className='create-account__span'>
        <span>Já tem um conta?</span>
        <Link to={"/login"}>Iniciar sessão</Link>
      </div>
    </section>
  )
}

export default Account;