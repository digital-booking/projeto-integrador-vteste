import React from 'react';
import './style.scss';
import { Link } from "react-router-dom";
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Field from '../../components/Form/Field';

const SignupSchema = Yup.object().shape({
  email: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

const Login = () => {
  return (
    <section className='login'>
      <h1>
        Iniciar sessão
      </h1>

      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={values => {
          // same shape as initial values
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <Form className='form-login'>
            <div className='input-group'>
              <Field 
              name="email" 
              type="email" 
              className="input-group__input" 
              label="E-mail"
              />
              {errors.email && touched.email ? <div className='field-errors'>{errors.email}</div> : null}
            </div>

            <div className='input-group'>
              <Field 
              name="password" 
              type="password" 
              className="input-group__input" 
              label='Senha'
              />
              {errors.password && touched.password ? <div className='field-errors'>{errors.password}</div> : null}
            </div>
        
            <button type="submit">Entrar</button>
          </Form>
        )}
      </Formik>

      <div className='login__span'>
        <span>Ainda não tem conta?</span>
        <Link to={"/account"}>Registre-se</Link>
      </div>
    </section>
  )
}

export default Login;