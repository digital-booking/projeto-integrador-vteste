import React from 'react';
import {useField} from 'formik';

const Field = ({label, ...props}) => {

  /* meta pode ser usado para mostrar msg de erro */
  const [inputProps, meta] = useField(props);
  const id = props.id || props.name;

  return (
    <>
    {label && <label htmlFor={id}>{label}</label>}
    <input id={id} {...inputProps} {...props} />
    </>
  )
}

export default Field;