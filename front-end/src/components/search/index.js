import React from 'react';
import './style.scss';
/* import { useState } from 'react';
import DatePicker from "react-datepicker"; */
/* import "react-datepicker/dist/react-datepicker.css"; */


const Search = () => {

  /* const [startDate, setStartDate] = useState(new Date()); */
  return (
    <section className='search'>
      <h1>Buscar ofertas em apartamentos, casas e muito mais</h1>

      <form action="" className='form-container'>
        <div className='form-container__input'>
          <svg width="18" height="26" viewBox="0 0 18 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.66075 12.4325C7.86535 12.4325 7.10254 12.1072 6.54011 11.5281C5.97768 10.9491 5.66171 10.1638 5.66171 9.34495C5.66171 8.52609 5.97768 7.74078 6.54011 7.16176C7.10254 6.58274 7.86535 6.25745 8.66075 6.25745C9.45615 6.25745 10.219 6.58274 10.7814 7.16176C11.3438 7.74078 11.6598 8.52609 11.6598 9.34495C11.6598 9.75041 11.5822 10.1519 11.4315 10.5265C11.2808 10.9011 11.0599 11.2414 10.7814 11.5281C10.5029 11.8148 10.1723 12.0423 9.80843 12.1974C9.44457 12.3526 9.05459 12.4325 8.66075 12.4325ZM8.66075 0.699951C6.43364 0.699951 4.29775 1.61076 2.72295 3.23201C1.14814 4.85326 0.263428 7.05215 0.263428 9.34495C0.263428 15.8287 8.66075 25.3999 8.66075 25.3999C8.66075 25.3999 17.0581 15.8287 17.0581 9.34495C17.0581 7.05215 16.1734 4.85326 14.5986 3.23201C13.0237 1.61076 10.8879 0.699951 8.66075 0.699951Z" fill="#263238" />
          </svg>
          <input type="text" name="destino" id="destino" placeholder='Onde vamos?'/>
        </div>
        {/*  <DatePicker
          className="datepicker"
          selected={startDate}
          showPreviousMonths
          onChange={(date) => setStartDate(date)}
          monthsShown={2}
        /> */}
        <div className='form-container__input'>
          <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.8682 1.89998H17.842V0.0999756H15.7895V1.89998H5.52734V0.0999756H3.4749V1.89998H2.44868C1.31984 1.89998 0.39624 2.70998 0.39624 3.69998V18.1C0.39624 19.09 1.31984 19.9 2.44868 19.9H18.8682C19.997 19.9 20.9206 19.09 20.9206 18.1V3.69998C20.9206 2.70998 19.997 1.89998 18.8682 1.89998ZM18.8682 18.1H2.44868V6.39998H18.8682V18.1Z" fill="#263238" />
          </svg>
          <input type="date" name="periodo" id="periodo" />
        </div>
        <button className='btn' type="submit">Buscar</button>
      </form>
    </section>
  )
}

export default Search;