import './style.scss';
import React, { useState, useEffect } from "react";

const Category = () => {

    const [categories, setCategories] = useState([]);
    
    useEffect(() => {
        fetch('./categories.json', {
        headers: {
                Accept: "application/json"
        }
        }).then(res => res.json())
        .then(res => setCategories(res.data))
    }, []);

    return (
        <div className='category__container'>
            <h2>Buscar por tipo de acomodação</h2>
            <div className='category__content'>
                {categories.map(category => (
                    <div className='category__item'>
                        <img src={category.image} alt="quartos compartilhados" />
                        <div className='category__text'>
                            <h3>{category.qualification}</h3>
                            <p>{category.description}</p>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Category;