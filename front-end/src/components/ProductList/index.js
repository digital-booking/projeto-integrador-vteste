import './style.scss';
import React, { useState, useEffect } from "react";

const ProductList = () => {

    const [products, setProducts] = useState([]);
    
    useEffect(() => {
        fetch('./productList.json', {
        headers: {
                Accept: "application/json"
        }
        }).then(res => res.json())
        .then(res => setProducts(res.data))
    }, []);

    return (
        <div className='productList__container'>
            <h2>Recomendações</h2>
            <div className='productList__content'>
                {products.map(product => (
                    <div className='productList__item'>
                        <div className='productList__img'>
                            <img src={product.image} alt="quartos compartilhados" />
                        </div>
                        <div className='productList__text'>
                            <p className='productList_category'>{product.category}</p>
                            <h3>{product.title}</h3>
                            <p>{product.localization}</p>
                            <p>{product.description}</p>
                            <button type='button'>Ver detalhes</button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ProductList;